class Maze:
    def __init__(self,offsets):
        ???

    def __next__(self):
        ???

    def has_escaped(self):
        ???

def num_steps(offsets):
    maze = Maze(offsets)
    steps = 0
    while True:
        maze = next(maze)
        steps += 1
        if maze.has_escaped():
            break
    return steps

offsets = ???
print(num_steps(offsets))
