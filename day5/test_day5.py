import unittest
from day5 import num_steps

class test_num_steps(unittest.TestCase):
    def test_offset_list(self):
        offset_list = [0,3,0,1,-3]
        self.assertEqual(num_steps(offset_list),5)
