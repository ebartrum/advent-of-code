from spiral_position import *
import numpy as np

BREAK_VALUE = 347991
position_generator = SpiralPosition()

def get_neighbours(current_position):
    x = current_position[0]
    y = current_position[1]
    result = [(x + i, y + j) for i in [-1,0,1] for j in [-1,0,1]] 
    return result

def get_update_value(current_position, position_values):
    neighbours = get_neighbours(current_position)
    neighbour_values = [position_values.get(x, 0) for x in neighbours]
    return np.sum(neighbour_values)

position_values = {(0,0): 1}
while True:
    current_position = next(position_generator)
    update_value = get_update_value(current_position, position_values)
    position_values[current_position] = update_value
    print(current_position, "assigned value", update_value)
    if update_value > BREAK_VALUE:
        break
