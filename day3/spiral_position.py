class SpiralPosition:
    def __init__(self, initial=(0,0)):
        self.x = initial[0]
        self.y = initial[1]
        self.square_depth = 0
    
    def __iter__(self):
        return self

    def __next__(self):
        
        if self.x == self.square_depth and self.y == - self.square_depth:
            self.x += 1
            self.square_depth += 1
        
        elif self.x == self.square_depth and self.y < self.square_depth:
            self.y += 1

        elif self.y == self.square_depth and self.x > - self.square_depth:
            self.x -= 1

        elif self.x == - self.square_depth and self.y > - self.square_depth:
            self.y -= 1

        elif self.y == - self.square_depth and self.x < self.square_depth:
            self.x += 1
        
        return (self.x, self.y)
