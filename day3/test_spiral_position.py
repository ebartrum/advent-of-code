import unittest
from spiral_position import *

class TestSpiralPosition(unittest.TestCase):
    def test_10(self):
        spiral_position = SpiralPosition()
        test_list = [next(spiral_position) for i in range(10)]
        self.assertEqual(test_list,[(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1), (2, -1), (2, 0)])
