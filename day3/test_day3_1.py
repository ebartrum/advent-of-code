import unittest
from day3_1 import *

class TestLayer(unittest.TestCase):
    def test_1(self):
        self.assertEqual(layer(1),0)
    def test_12(self):
        self.assertEqual(layer(12),2)
    def test_23(self):
        self.assertEqual(layer(23),2)

class TestLargestLayerValue(unittest.TestCase):
    def test_1(self):
        self.assertEqual(largest_layer_value(1),1)
    def test_12(self):
        self.assertEqual(largest_layer_value(12),25)
    def test_23(self):
        self.assertEqual(largest_layer_value(23),25)

class TestLayerMidpoints(unittest.TestCase):
    def test_1(self):
        self.assertEqual(layer_midpoints(1),[1.0,1.0,1.0,1.0])
    def test_2(self):
        self.assertEqual(layer_midpoints(2),[2.0,4.0,6.0,8.0])
    def test_12(self):
        self.assertEqual(layer_midpoints(12),[11.0,15.0,19.0,23.0])
    def test_23(self):
        self.assertEqual(layer_midpoints(23),[11.0,15.0,19.0,23.0])

class TestDistanceToMidpoint(unittest.TestCase):
    def test_1(self):
        self.assertEqual(distance_to_midpoint(1),0)
    def test_12(self):
        self.assertEqual(distance_to_midpoint(12),1)
    def test_21(self):
        self.assertEqual(distance_to_midpoint(21),2)

class TestSteps(unittest.TestCase):
    def test_1(self):
        self.assertEqual(steps(1),0)
    def test_12(self):
        self.assertEqual(steps(12),3)
    def test_23(self):
        self.assertEqual(steps(23),2)
    def test_1024(self):
        self.assertEqual(steps(1024),31)
