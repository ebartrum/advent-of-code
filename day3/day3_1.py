import numpy as np

def steps(n):
    return layer(n) + distance_to_midpoint(n)

def layer(n):
    sqrt = n ** 0.5
    return np.ceil((sqrt - 1) / 2)

def layer_side_length(n):
    return 2*layer(n) + 1

def largest_layer_value(n):
    return layer_side_length(n) ** 2

def layer_midpoints(n):
    side_length = layer_side_length(n)
    largest_value = largest_layer_value(n)
    largest_midpoint = largest_value - (side_length - 1)/2
    return [largest_midpoint - i*(side_length -1) for i in range(4)][::-1]

def distance_to_midpoint(n):
    midpoints = layer_midpoints(n)
    distances = [np.abs(n-m) for m in midpoints]
    return np.min(distances)

input = 347991
print("Result: ",steps(input))
