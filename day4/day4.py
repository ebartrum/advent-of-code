FILENAME = "data/day4.txt"
file = open(FILENAME, "r")

ANAGRAM_CONSTRAINT = True

def contains_duplicates(list):
    seen = set()
    for item in list:
        if item in seen:
            return True
        seen.add(item)
    return False

def alphabetical(x):
    return ''.join(sorted(x))

def is_valid(string, anagram_constraint=False):
    list = string.split(" ")
    if anagram_constraint:
        list = [alphabetical(x) for x in list]
    return not contains_duplicates(list)

count = 0
for line in file.readlines():
    line = line.strip('\n')
    if(is_valid(line, ANAGRAM_CONSTRAINT)):
        count += 1
print(count)
