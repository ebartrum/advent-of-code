import unittest
from day4 import is_valid, contains_duplicates

class test_contains_duplicates(unittest.TestCase):
    def test_list_without_duplicates(self):
        list = [1,2,3]
        self.assertFalse(contains_duplicates(list))
        
    def test_list_with_duplicates(self):
        list = [1,2,3,1]
        self.assertTrue(contains_duplicates(list))

    def test_string_list_with_duplicates(self):
        list = ["a","a"]
        self.assertTrue(contains_duplicates(list))

    def test_string_list2_with_duplicates(self):
        list = ["a","b","a"]
        self.assertTrue(contains_duplicates(list))

    def test_string_list3_with_duplicates(self):
        list = ["aa","ba","ca","ba","da"]
        self.assertTrue(contains_duplicates(list))

class test_is_valid(unittest.TestCase):
    def test_valid_string(self):
        string = "aa bb cc dd ee"
        self.assertTrue(is_valid(string))

    def test_invalid_string(self):
        string = "aa bb cc dd aa"
        self.assertFalse(is_valid(string))

    def test_second_valid_string(self):
        string = "aa bb cc dd aaa"
        self.assertTrue(is_valid(string))

class test_is_valid_with_anagram_condition(unittest.TestCase):
    def test_valid_anagram_string(self):
        string = "abcde fghij"
        self.assertTrue(is_valid(string, anagram_constraint = True))

    def test_invalid_anagram_string(self):
        string = "abcde xyz ecdab"
        self.assertFalse(is_valid(string, anagram_constraint = True))

    def test_valid_anagram_string2(self):
        string = "a ab abc abd abf abj"
        self.assertTrue(is_valid(string, anagram_constraint = True))

    def test_valid_anagram_string3(self):
        string = "iiii oiii ooii oooi oooo"
        self.assertTrue(is_valid(string, anagram_constraint = True))

    def test_invalid_anagram_string2(self):
        string = "oiii ioii iioi iiio"
        self.assertFalse(is_valid(string, anagram_constraint = True))
