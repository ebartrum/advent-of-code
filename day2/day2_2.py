import pandas as pd
import numpy as np

data = pd.read_csv('data/day2.txt',delimiter="\t", header=None)

def get_divisible(list):
    quotients = [float(y) / float(z) for y in list for z in list]
    answer = next(x for x in quotients if x.is_integer() and x > 1)
    return answer

divisions = data.apply(get_divisible, axis=1)
answer = np.sum(divisions)

print(answer)
