import pandas as pd
import numpy as np

data = pd.read_csv('data/day2.txt',delimiter="\t", header=None)

differences = data.apply(lambda x: np.max(x) - np.min(x), axis=1)
answer = np.sum(differences)

print(answer)
